use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

#[derive(Debug)]
enum DError {
    StrError(String),
    IoError(io::Error),
}

impl From<io::Error> for DError {
    fn from(err: io::Error) -> DError {
        DError::IoError(err)
    }
}

impl From<String> for DError {
    fn from(err: String) -> DError {
        DError::StrError(err)
    }
}

pub fn resolve (file: &str) -> Result<u32, String> {
    Ok(file_to_vector(file).unwrap())
}

fn file_to_vector(file: &str) -> Result<u32, DError> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut map: HashMap<String, Vec<String>> = HashMap::new();
    for line in s.lines() {
        let (row, col) = line.split_at(7);

        if !map.contains_key(&row.to_string()) {
            map.insert(row.to_string(), Vec::new());
        }

        map.get_mut(&row.to_string()).unwrap().push(col.to_string());
    }

    for (row, cols) in map {
        if cols.iter().count() == 7 {
            let mut cols_v: Vec<String> = Vec::new();
            for x in vec!["L", "R"] {
                for y in vec!["L", "R"] {
                    for z in vec!["L", "R"] {
                        cols_v.push(format!("{}{}{}", x, y, z));
                    }
                }
            }

            for col in cols_v {
                if !cols.contains(&col) {
                    return Ok(board_decode(format!("{}{}", row, col)));
                }
            }
        }
    }

    Err(DError::StrError("Error".to_string()))
}

fn board_decode(board: String) -> u32 {
    let mut fb_min: u32 = 0;
    let mut fb_max: u32 = 127;
    let mut lr_min: u32 = 0;
    let mut lr_max: u32 = 7;

    for character in board.chars() {
        match character {
            'F' => fb_max = (fb_max - fb_min) / 2 + fb_min,
            'B' => fb_min = (fb_max - fb_min) / 2 + fb_min,
            'L' => lr_max = (lr_max - lr_min) / 2 + lr_min,
            'R' => lr_min = (lr_max - lr_min) / 2 + lr_min,
            _ => ()
        }
    }

    fb_max * 8 + lr_max
}

