use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u32, String> {
    match file_to_vector(file).unwrap().iter().max() {
        Some(max) => return Ok(*max),
        None => return Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Vec<u32>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u32> = Vec::new();
    for line in s.lines() {
        vector.push(board_decode(line.to_string()));
    }

    Ok(vector)
}

fn board_decode(board: String) -> u32 {
    let mut fb_min: u32 = 0;
    let mut fb_max: u32 = 127;
    let mut lr_min: u32 = 0;
    let mut lr_max: u32 = 7;

    for character in board.chars() {
        match character {
            'F' => fb_max = (fb_max - fb_min) / 2 + fb_min,
            'B' => fb_min = (fb_max - fb_min) / 2 + fb_min,
            'L' => lr_max = (lr_max - lr_min) / 2 + lr_min,
            'R' => lr_min = (lr_max - lr_min) / 2 + lr_min,
            _ => ()
        }
    }

    fb_max * 8 + lr_max
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day05/example").is_ok());
    }

    #[test]
    fn resolution_is_820() {
        assert_eq!(820, resolve("./src/day05/example").unwrap());
    }
}
