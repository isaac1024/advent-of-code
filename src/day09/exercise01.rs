use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str, preanble: u32) -> Result<u64, String> {
    let numbers = file_to_vector(file).unwrap();
    let mut position = preanble;

    loop {
        let start = position - preanble;
        let end = position;
        match recursive_search(numbers.get(start as usize..end as usize).unwrap().to_vec(), *numbers.get(position as usize).unwrap(), 2) {
            Ok(_) => (),
            Err(_) => return Ok(*numbers.get(position as usize).unwrap()),
        }

        position += 1;
    }
}

fn file_to_vector(file: &str) -> Result<Vec<u64>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u64> = Vec::new();
    for number in s.lines() {
        vector.push(number.parse::<u64>().unwrap());
    }

    Ok(vector)
}

fn recursive_search (mut vector: Vec<u64>, number_to_search: u64, recursivity: u32) -> Result<Vec<u64>, String> {
    let mut finded_number;
    finded_number = Vec::new();

    if recursivity > 1 {
        let mut first_number;
        let mut second_number;

        loop {
            if vector.is_empty() {
                return Err("Elements not finded".to_string());
            }

            first_number = vector.remove(0);

            if first_number > number_to_search {
                continue;
            }

            second_number = number_to_search - first_number;

            let mut result = match recursive_search(vector.clone(), second_number, recursivity - 1) {
                Ok(v) => v,
                Err(_e) => continue,
            };

            finded_number.append(&mut result);
            finded_number.push(first_number);

            return Ok(finded_number);
        }
    } else {
        if vector.contains(&number_to_search) {
            finded_number.push(number_to_search);

            return Ok(finded_number);
        }
    }

    Err("Elements not finded".to_string())
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day09/example").is_ok());
    }

    #[test]
    fn resolution_is_127() {
        assert_eq!(127, resolve("./src/day09/example", 5).unwrap());
    }
}
