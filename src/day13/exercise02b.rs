use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

pub fn resolve (file: &str) -> Result<u64, String> {
    let map = file_to_vector(file).unwrap();

    let (first, _) = map.iter().min_by(|(_, a_val), (_, b_val)| a_val.cmp(b_val)).unwrap();

    let mut c = 1;
    for (k, v) in map.iter() {
        if first == v {
            c = first * k;
        }
    }

    let mut x = *first;
    let mut res = 0;
    let mut old_x = x;
    let mut old_res = res;
    let mut counter = 1;

    for (key, value) in map {
        if value == 0 {
            continue;
        }
        counter = 1;

        loop {
            if (x*counter+res+value)%key == 0 {
                break;
            }

            counter += 1;
        }

        old_x = x;
        old_res = res;

        x *= key;
        res = old_x*counter+old_res;
    }

    Ok(old_x*counter+old_res)
}

fn file_to_vector(file: &str) -> Result<HashMap<u64, u64>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut map: HashMap<u64, u64> = HashMap::new();

    let mut timer = 0;
    for number in s.lines().nth(1).unwrap().split(',') {
        if number != "x" {
            let n = number.parse::<u64>().unwrap();
            map.insert(n, timer);
        }

        timer += 1;
    }

    Ok(map)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day13/example").is_ok());
    }

    #[test]
    fn resolution_is_1068781() {
        assert_eq!(1068781, resolve("./src/day13/example").unwrap());
    }
}
