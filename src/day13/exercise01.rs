use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

pub fn resolve (file: &str) -> Result<u64, String> {
    let (number, vector) = file_to_vector(file).unwrap();

    let mut map: HashMap<u32, u32> = HashMap::new();

    for n in vector {
        let modulo = number % n;

        match modulo {
            0 => map.insert(n, 0),
            _ => map.insert(n, n - modulo),
        };
    }

    let (key, value) = map.iter().min_by(|(_, a_value), (_, b_value)| a_value.cmp(b_value)).unwrap();

    Ok((key * value) as u64)
}

fn file_to_vector(file: &str) -> Result<(u32, Vec<u32>), io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let number = s.lines().next().unwrap().parse::<u32>().unwrap();

    let vector: Vec<u32> = s.lines().nth(1).unwrap().split(',').map(|n| n.parse::<u32>()).filter_map(Result::ok).collect();

    Ok((number, vector))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day13/example").is_ok());
    }

    #[test]
    fn resolution_is_295() {
        assert_eq!(295, resolve("./src/day13/example").unwrap());
    }
}
