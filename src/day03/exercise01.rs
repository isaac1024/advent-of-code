use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u64, String> {
    let mut trees: u64 = 0;
    let mut col: usize = 0;

    let rows: Vec<Vec<bool>> = match file_to_vector(file) {
        Ok(v) => v,
        Err(_e) => return Err("Error".to_string()),
    };

    for row in rows {
        let row_len = row.iter().count();
        if col >= row_len {
            col = col - row_len;
        }

        trees += row[col] as u64;

        col += 3;
    }

    Ok(trees)
}

fn file_to_vector(file: &str) -> Result<Vec<Vec<bool>>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut rows: Vec<Vec<bool>> = Vec::new();
    for line in s.lines() {
        let mut cols: Vec<bool> = Vec::new();
        for character in line.chars() {
            cols.push(character == '#');
        }
        rows.push(cols);
    }

    Ok(rows)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day03/example").is_ok());
    }

    #[test]
    fn resolution_is_7() {
        assert_eq!(7, resolve("./src/day03/example").unwrap());
    }
}
