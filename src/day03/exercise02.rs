use std::io;
use std::io::Read;
use std::fs::File;

struct Trajectory {
    right: usize,
    down: usize,
}

pub fn resolve (file: &str) -> Result<u64, String> {
    let mut result: u64 = 1;

    let mut travels: Vec<Trajectory> = Vec::new();
    travels.push(Trajectory {right: 1, down: 1});
    travels.push(Trajectory {right: 3, down: 1});
    travels.push(Trajectory {right: 5, down: 1});
    travels.push(Trajectory {right: 7, down: 1});
    travels.push(Trajectory {right: 1, down: 2});

    let rows: Vec<Vec<bool>> = match file_to_vector(file) {
        Ok(v) => v,
        Err(_e) => return Err("Error".to_string()),
    };

    let row_len = rows.iter().count();

    for travel in travels {
        let mut trees: u64 = 0;
        let mut row: usize = 0;
        let mut col: usize = 0;

        while row < row_len {
            let row_len = rows[row].iter().count();
            if col >= row_len {
                col = col - row_len;
            }

            trees += rows[row][col] as u64;

            row += travel.down;
            col += travel.right;
        }

        result *= trees;
    }

    Ok(result)
}

fn file_to_vector(file: &str) -> Result<Vec<Vec<bool>>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut rows: Vec<Vec<bool>> = Vec::new();
    for line in s.lines() {
        let mut cols: Vec<bool> = Vec::new();
        for character in line.chars() {
            cols.push(character == '#');
        }
        rows.push(cols);
    }

    Ok(rows)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day03/example").is_ok());
    }

    #[test]
    fn resolution_is_336() {
        assert_eq!(336, resolve("./src/day03/example").unwrap());
    }
}
