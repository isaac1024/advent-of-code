use std::env;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

fn main() {
    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "01" => day01::resolve(args[2].as_str()),
        "02" => day02::resolve(args[2].as_str()),
        "03" => day03::resolve(args[2].as_str()),
        "04" => day04::resolve(args[2].as_str()),
        "05" => day05::resolve(args[2].as_str()),
        "06" => day06::resolve(args[2].as_str()),
        "07" => day07::resolve(args[2].as_str()),
        "08" => day08::resolve(args[2].as_str()),
        "09" => day09::resolve(args[2].as_str()),
        "10" => day10::resolve(args[2].as_str()),
        "11" => day11::resolve(args[2].as_str()),
        "12" => day12::resolve(args[2].as_str()),
        "13" => day13::resolve(args[2].as_str()),
        "14" => day14::resolve(args[2].as_str()),
        "15" => day15::resolve(args[2].as_str()),
        _ => println!("Invalid day"),
    }
}
