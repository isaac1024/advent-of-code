use std::io;
use std::io::Read;
use std::fs::File;

use regex::Regex;

#[derive(Debug)]
struct Bag {
    name: String,
    number_of_bags: u32,
    children: Vec<Bag>,
}


impl Bag {
    fn new(name: String, number_of_bags: u32, children: Vec<Bag>) -> Bag {
        Bag {name, number_of_bags, children}
    }

    fn sum(self: Self) -> u32 {
        let mut suma: u32 = self.number_of_bags;

        for b in self.children {
            suma += self.number_of_bags * b.sum();
        }

        suma
    }
}

pub fn resolve (file: &str) -> Result<u32, String>{
    match file_to_vector(file) {
        Ok(b) => return Ok(b.sum() - 1),
        _ => return Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Bag, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    Ok(search("shiny gold".to_string(), 1, s).unwrap())
}

fn search(bag_str: String, nbags: u32, s: String) -> Option<Bag> {
    let mut vector: Vec<Bag> = Vec::new();
    let bags = Regex::new(format!(r"^{}", bag_str).as_str()).unwrap();
    let bag = Regex::new(r"(?P<nbags>\d+) (?P<bag>\w+ \w+ bag)").unwrap();
    let matches = s.lines().filter(|line| bags.is_match(line));

    for i in matches {
        for r in bag.captures_iter(i) {
            match search(r["bag"].to_string(), r["nbags"].parse::<u32>().unwrap(), s.clone()) {
                Some(b) => vector.push(b),
                None => (),
            }
        }
    }

    Some(Bag::new(bag_str, nbags, vector))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day07/example2").is_ok());
    }

    #[test]
    fn resolution_is_126() {
        assert_eq!(126, resolve("./src/day07/example2").unwrap());
    }
}
