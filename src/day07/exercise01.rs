use std::io;
use std::io::Read;
use std::fs::File;

use regex::Regex;
use regex::RegexSet;

pub fn resolve (file: &str) -> Result<u32, String> {
    Ok(file_to_vector(file).unwrap())
}

fn file_to_vector(file: &str) -> Result<u32, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<String> = Vec::new();
    vector.push(r"\d+ shiny gold bag".to_string());

    let bag = Regex::new(r"^\w+ \w+").unwrap();

    let mut return_value;
    loop {
        let re = RegexSet::new(vector.clone()).unwrap();
        let aux = s.clone();
        let matches = aux.lines().filter(|line| re.is_match(line));
        return_value = true;

        for i in matches.clone() {
            s = s.replace(i, "");
            let new_re = format!(r"\d+ {} bag", bag.captures(i).unwrap().get(0).unwrap().as_str());
            if !vector.contains(&new_re) {
                vector.push(new_re);
                return_value = false;
            }
        }

        if return_value {
            return Ok((vector.iter().count() - 1) as u32);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day07/example").is_ok());
    }

    #[test]
    fn resolution_is_4() {
        assert_eq!(4, resolve("./src/day07/example").unwrap());
    }
}
