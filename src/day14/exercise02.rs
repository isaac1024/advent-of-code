use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

pub fn resolve (file: &str) -> Result<u64, String> {
    let vector = file_to_vector(file).unwrap();

    let result = vector.iter().sum();

    Ok(result)
}

fn file_to_vector(file: &str) -> Result<Vec<u64>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut mask: Vec<char> = Vec::new();
    let mut map: HashMap<u64, u64> = HashMap::new();
    let mut key: Vec<char>;
    for line in s.lines() {
        let split: Vec<&str> = line.split(" = ").collect();
        let action = split[0];
        let number = split[1];

        if action == "mask" {
            mask = number.chars().collect();
            continue;
        }

        key = decimal_to_vector(action[4..action.len()-1].parse::<u64>().unwrap());

        for i in 0..36 {
            match mask[i] {
                '1' => key[i] = '1',
                'X' => key[i] = 'X',
                _ => (),
            }
        }

        for address in address_to_vector(key) {
            map.insert(vector_to_decimal(address), number.parse::<u64>().unwrap());
        }
    }

    Ok(map.values().cloned().collect())
}

fn address_to_vector(address: Vec<char>) -> Vec<Vec<char>> {
    let mut positions:Vec<usize> = Vec::new();
    let mut vector: Vec<Vec<char>> = Vec::new();

    for i in 0..36 {
        if address[i] == 'X' {
            positions.push(i);
        }
    }

    vector.push(address);

    if positions.len() == 0 {
        return vector;
    }

    for p in positions {
        let mut aux_vector: Vec<Vec<char>> = Vec::new();
        for v in vector {
            for n in ['0', '1'].iter() {
                let mut a = v.clone();
                a[p] = *n;
                aux_vector.push(a);
            }
        }
        vector = aux_vector;
    }

    vector
}

fn decimal_to_vector(mut decimal: u64) -> Vec<char> {
    let mut vector: Vec<char> = Vec::new();

    while decimal > 0 {
        if decimal%2 == 0 {
            vector.insert(0, '0');
        } else {
            vector.insert(0, '1');
        }

        decimal /= 2;
    }

    while vector.len() < 36 {
        vector.insert(0, '0');
    }

    vector
}

fn vector_to_decimal(mut vector: Vec<char>) -> u64 {
    let mut decimal: u64 = 0;

    while vector.len() > 0 {
        if vector.remove(0) == '1' {
            decimal += 2_u64.pow(vector.len() as u32);
        }
    }

    decimal
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_decimal_to_vector_10() {
        assert_eq!(decimal_to_vector(10), ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0']);
    }

    #[test]
    fn check_decimal_to_vector_11() {
        assert_eq!(decimal_to_vector(11), ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1']);
    }

    #[test]
    fn check_vector_to_decimal_10() {
        assert_eq!(vector_to_decimal(['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'].to_vec()), 10);
    }

    #[test]
    fn check_vector_to_decimal_11() {
        assert_eq!(vector_to_decimal(['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1'].to_vec()), 11);
    }

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day14/example2").is_ok());
    }

    #[test]
    fn resolution_is_208() {
        assert_eq!(208, resolve("./src/day14/example2").unwrap());
    }
}
