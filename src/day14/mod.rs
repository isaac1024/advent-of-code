mod exercise01;
mod exercise02;

pub fn resolve (exercise: &str) {
    let result: Result<u64, String> = match exercise {
        "1" => exercise01::resolve("./src/day14/input"),
        "2" => exercise02::resolve("./src/day14/input"),
        _ => Err("Invalid exercise".to_string()),
    };

    match result {
        Ok(n) => println!("Result: {}", n),
        Err(e) => println!("{}", e),
    }
}
