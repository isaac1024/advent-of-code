mod exercise01;
mod exercise02;

use std::error::Error;

pub fn resolve (exercise: &str) {
    let result: Result<u32, Box<dyn Error>> = match exercise {
        "1" => exercise01::resolve("./src/day01/input"),
        "2" => exercise02::resolve("./src/day01/input"),
        _ => Err("Invalid exercise".into()),
    };

    match result {
        Ok(n) => println!("Result: {}", n),
        Err(e) => println!("{}", e),
    }
}
