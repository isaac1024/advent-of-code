use std::error::Error;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u32, Box<dyn Error>> {
    let mut numbers = file_to_vector(file)?;
    let mut first_number;
    let mut second_number;
    let mut finded_number;

    finded_number = false;

    loop {
        first_number = numbers.remove(0);

        second_number = 2020 - first_number;

        if numbers.contains(&second_number) {
            finded_number = true;
            break;
        }

        if numbers.is_empty() {
            break;
        }
    }

    if !finded_number {
        return Err("Number not finded".into());
    }

    Ok(first_number * second_number)
}

fn file_to_vector(file: &str) -> Result<Vec<u32>, Box<dyn Error>> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u32> = Vec::new();
    for number in s.lines() {
        vector.push(number.parse::<u32>()?);
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day01/example").is_ok());
    }

    #[test]
    fn resolution_is_514579() -> Result<(), Box<dyn Error>> {
        assert_eq!(514579, resolve("./src/day01/example")?);
        Ok(())
    }
}
