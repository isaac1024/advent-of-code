use std::error::Error;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u32, Box<dyn Error>> {
    let numbers = file_to_vector(file)?;

    let result = recursive_search(numbers, 2020, 3)?;

    let mut final_number;
    final_number = 1;

    for v in &result {
        final_number = final_number * v;
    }

    Ok(final_number)
}

fn recursive_search (mut vector: Vec<u32>, number_to_search: u32, recursivity: u32) -> Result<Vec<u32>, Box<dyn Error>> {
    let mut finded_number;
    finded_number = Vec::new();

    if recursivity > 1 {
        let mut first_number;
        let mut second_number;

        loop {
            if vector.is_empty() {
                return Err("Elements not finded".into());
            }

            first_number = vector.remove(0);

            if first_number > number_to_search {
                continue;
            }

            second_number = number_to_search - first_number;

            let mut result = match recursive_search(vector.clone(), second_number, recursivity - 1) {
                Ok(v) => v,
                Err(_e) => continue,
            };

            finded_number.append(&mut result);
            finded_number.push(first_number);

            return Ok(finded_number);
        }
    } else {
        if vector.contains(&number_to_search) {
            finded_number.push(number_to_search);

            return Ok(finded_number);
        }
    }

    Err("Elements not finded".into())
}

fn file_to_vector(file: &str) -> Result<Vec<u32>, Box<dyn Error>> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u32> = Vec::new();
    for number in s.lines() {
        vector.push(number.parse::<u32>()?);
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day01/example").is_ok());
    }

    #[test]
    fn resolution_is_241861950() -> Result<(), Box<dyn Error>> {
        assert_eq!(241861950, resolve("./src/day01/example")?);
        Ok(())
    }
}
