use std::io;
use std::io::Read;
use std::fs::File;
use std::cell::Cell;

struct Game {
    command: String,
    acction: i32,
    executed: Cell<bool>,
}

impl Game {
    fn new(command: String, acction: i32, executed: bool) -> Game {
        Game {command, acction, executed: Cell::new(executed)}
    }

    fn is_executed(&self) -> bool {
        self.executed.get()
    }

    fn execute(&self) {
        self.executed.set(true);
    }
}

pub fn resolve (file: &str) -> Result<i32, String> {
    let mut position: i32 = 0;
    let mut counter: i32 = 0;
    let games: Vec<Game> = file_to_vector(file).unwrap();

    loop {
        let game: &Game = &games[position as usize];

        if game.is_executed() == true {
            break;
        }

        game.execute();

        match game.command.as_str() {
            "nop" => position += 1,
            "acc" => {position += 1; counter += game.acction;},
            "jmp" => position += game.acction,
            _ => ()
        }
    }

    Ok(counter as i32)
}

fn file_to_vector(file: &str) -> Result<Vec<Game>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Game> = Vec::new();
    for line in s.lines() {
        let game = line.split(' ').collect::<Vec<&str>>();
        vector.push(Game::new(game[0].to_string(), game[1].parse::<i32>().unwrap(), false));
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day08/example").is_ok());
    }

    #[test]
    fn resolution_is_5() {
        assert_eq!(5, resolve("./src/day08/example").unwrap());
    }
}
