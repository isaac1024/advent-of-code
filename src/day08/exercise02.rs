use std::io;
use std::io::Read;
use std::fs::File;
use std::cell::Cell;

#[derive(Debug)]
struct Game {
    command: String,
    acction: i32,
    executed: Cell<bool>,
    changed: Cell<bool>,
}

impl Game {
    fn new(command: String, acction: i32) -> Game {
        Game {command, acction, executed: Cell::new(false), changed: Cell::new(false)}
    }

    fn is_executed(&self) -> bool {
        self.executed.get()
    }

    fn execute(&self) {
        self.executed.set(true);
    }

    fn unexecute(&self) {
        self.executed.set(false);
    }

    fn is_changed(&self) -> bool {
        self.changed.get()
    }

    fn change(&self) {
        self.changed.set(true)
    }
}

pub fn resolve (file: &str) -> Result<i32, String> {
    let mut position: i32 = 0;
    let mut counter: i32 = 0;
    let mut changed: bool = false;
    let games: Vec<Game> = file_to_vector(file).unwrap();

    loop {
        if position as usize >= games.iter().count() {
            break;
        }

        let game: &Game = &games[position as usize];

        if game.is_executed() {
            position = 0;
            counter = 0;
            changed = false;

            games.iter().for_each(|g| g.unexecute());

            continue;
        }

        game.execute();

        match game.command.as_str() {
            "nop" => {
                if game.is_changed() || changed || game.acction == 0 {
                    position += 1;
                } else {
                    changed = true;
                    game.change();
                    position += game.acction;
                }
            },
            "acc" => {position += 1; counter += game.acction;},
            "jmp" => {
                if game.is_changed() || changed {
                    position += game.acction;
                } else {
                    changed = true;
                    game.change();
                    position += 1;
                }
            },
            _ => ()
        }
    }

    Ok(counter as i32)
}

fn file_to_vector(file: &str) -> Result<Vec<Game>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Game> = Vec::new();
    for line in s.lines() {
        let game = line.split(' ').collect::<Vec<&str>>();
        vector.push(Game::new(game[0].to_string(), game[1].parse::<i32>().unwrap()));
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day08/example").is_ok());
    }

    #[test]
    fn resolution_is_8() {
        assert_eq!(8, resolve("./src/day08/example").unwrap());
    }
}
