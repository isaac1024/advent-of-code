use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u64, String> {
    let mut vector: Vec<Vec<char>> = file_to_vector(file).unwrap();
    let mut old_vector: Vec<Vec<char>>;

    loop {
        old_vector = vector.clone();
        vector = change(vector);

        if old_vector == vector {
            break;
        }
    }

    let mut counter: u64 = 0;

    for v in vector {
        for c in v {
            if c == '#' {
                counter += 1;
            }
        }
    }

    Ok(counter)
}

fn change(vector: Vec<Vec<char>>)-> Vec<Vec<char>> {
    let r_len = vector.len();
    let c_len = vector[0].len();
    let adjacent = vec![-1, 0, 1];
    let mut new_vector = vector.clone();

    let mut y_pos = 0;

    while y_pos < r_len {
        let mut x_pos = 0;

        while x_pos < c_len {
            match vector[y_pos][x_pos] {
                'L' => {
                    let mut change = true;
                    'main: for y in adjacent.iter() {
                        let y_new = y_pos as i32 + y;

                        if y_new < 0 || y_new >= r_len as i32 {
                            continue;
                        }

                        for x in adjacent.iter() {
                            let x_new = x_pos as i32 + x;
                            if x_new  < 0 || x_new >= c_len as i32 {
                                continue;
                            }

                            if vector[y_new as usize][x_new as usize] == '#' {
                                change = false;
                                break 'main;
                            }
                        }
                    }

                    if change {
                        new_vector[y_pos][x_pos] = '#';
                    }
                },
                '#' => {
                    let mut counter = 0;
                    for y in adjacent.iter() {
                        let y_new = y_pos as i32 + y;

                        if y_new < 0 || y_new >= r_len as i32 {
                            continue;
                        }

                        for x in adjacent.iter() {
                            let x_new = x_pos as i32 + x;
                            if x_new  < 0 || x_new >= c_len as i32 {
                                continue;
                            }

                            if vector[y_new as usize][x_new as usize] == '#' {
                                counter += 1;
                            }
                        }
                    }

                    if counter >= 5 {
                        new_vector[y_pos][x_pos] = 'L';
                    }
                },
                _ => {x_pos += 1; continue;},
            }
            x_pos += 1;
        }

        y_pos += 1;
    }

    new_vector
}

fn file_to_vector(file: &str) -> Result<Vec<Vec<char>>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Vec<char>> = Vec::new();
    for line in s.lines() {
        let mut col: Vec<char> = Vec::new();

        for c in line.chars() {
            col.push(c);
        }

        vector.push(col);
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day11/example").is_ok());
    }

    #[test]
    fn resolution_is_37() {
        assert_eq!(37, resolve("./src/day11/example").unwrap());
    }
}
