mod exercise01;
mod exercise02;
mod exercise02b;

pub fn resolve (exercise: &str) {
    let result: Result<u64, String> = match exercise {
        "1" => exercise01::resolve("./src/day10/input"),
        "2" => exercise02::resolve("./src/day10/input"),
        "2b" => exercise02b::resolve("./src/day10/input"),
        _ => Err("Invalid exercise".to_string()),
    };

    match result {
        Ok(n) => println!("Result: {}", n),
        Err(e) => println!("{}", e),
    }
}
