use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u64, String> {
    let mut numbers = match file_to_vector(file) {
        Ok(vector) => vector,
        Err(_e) => return Err("Couldn't convert file to vector".to_string()),
    };

    numbers.sort();

    let mut jolt_1 = 0;
    let mut jolt_3 = 1;
    let mut prev = 0;

    for n in numbers {
        match n - prev {
            1 => jolt_1 += 1,
            3 => jolt_3 += 1,
            _ => (),
        }

        prev = n;
    }

    Ok(jolt_1 * jolt_3)
}

fn file_to_vector(file: &str) -> Result<Vec<u32>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u32> = Vec::new();
    for number in s.lines() {
        vector.push(number.parse::<u32>().unwrap());
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day10/example").is_ok());
    }

    #[test]
    fn resolution_is_220() {
        assert_eq!(220, resolve("./src/day10/example").unwrap());
    }
}
