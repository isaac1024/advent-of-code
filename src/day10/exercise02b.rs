use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u64, String> {
    let mut numbers = match file_to_vector(file) {
        Ok(vector) => vector,
        Err(_e) => return Err("Couldn't convert file to vector".to_string()),
    };

    numbers.sort();

    let mut counter = 1;
    let mut desde = 0;
    let mut hasta = 3;
    let mut prev = 0;
    let fin = numbers.len();

    while hasta < fin {
        if desde > 0 {
            prev = numbers[desde-1];
        }

        if hasta + 1 >= fin {
            counter *= check_remove_element(numbers[desde..].to_vec(), prev) + 1;
            break;
        }

        if numbers[hasta] - numbers[hasta-1] >= 3 && numbers[hasta-1] - numbers[hasta-2] >= 3 {
            counter *= check_remove_element(numbers[desde..hasta].to_vec(), prev) + 1;
            desde = hasta;
        }

        hasta += 1;
    }

    Ok(counter)
}

fn check_remove_element(mut vector: Vec<u32>, prev: u32) -> u64 {
    let mut index = 0;
    let mut counter: u64 = 0;
    let mut aux_prev = prev;
    let v_size = vector.len();

    while index < v_size - 1 {
        let num = vector.remove(index);

        if index > 0 {
            aux_prev = vector[index-1];
        }

        if vector[index] - aux_prev > 3 {
            vector.insert(index, num);
            index += 1;
            continue;
        }

        counter += 1;

        counter += check_remove_element(vector[index..].to_vec(), aux_prev);

        vector.insert(index, num);
        index += 1;
    }

    counter
}

fn file_to_vector(file: &str) -> Result<Vec<u32>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<u32> = Vec::new();
    for number in s.lines() {
        vector.push(number.parse::<u32>().unwrap());
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day10/example").is_ok());
    }

    #[test]
    fn resolution_is_19208() {
        assert_eq!(19208, resolve("./src/day10/example").unwrap());
    }
}
