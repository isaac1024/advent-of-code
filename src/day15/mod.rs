mod exercise01;
mod exercise02;

pub fn resolve (exercise: &str) {
    let input: Vec<usize> = vec![17, 1, 3, 16, 19, 0];
    let result: Result<usize, String> = match exercise {
        "1" => Ok(exercise01::resolve(input)),
        "2" => Ok(exercise02::resolve(input)),
        _ => Err("Invalid exercise".to_string()),
    };

    match result {
        Ok(n) => println!("Result: {}", n),
        Err(e) => println!("{}", e),
    }
}
