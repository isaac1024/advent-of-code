use std::collections::HashMap;

pub fn resolve (input: Vec<usize>) -> usize {
    let mut map: HashMap<usize, Vec<usize>> = HashMap::new();
    let mut i = 0;
    let mut last_number: usize;
    last_number = 0;

    for number in input.iter() {
        insert_to_map(&mut map, *number, i);
        last_number = *number;
        i += 1;
    }

    for i in input.len()..2020 {
        let vector = map.get(&last_number).unwrap();

        if vector.len() > 1{
            last_number = get_index_diff(vector);
        } else {
            last_number = 0;
        }

        insert_to_map(&mut map, last_number, i)
    }

    last_number
}

fn get_index_diff(vector: &Vec<usize>) -> usize {
    let last = vector.get(vector.len() - 1).unwrap();
    let second_last = vector.get(vector.len() - 2).unwrap();

    last - second_last
}

fn insert_to_map(map: &mut HashMap<usize, Vec<usize>>, number: usize, index: usize) {
    if let Some(vector) = map.get_mut(&number) {
        vector.push(index);
    } else {
        map.insert(number, vec![index]);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn resolution_is_436() {
        assert_eq!(436, resolve(vec![0, 3, 6]));
    }
}
