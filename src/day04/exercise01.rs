use std::io;
use std::io::Read;
use std::fs::File;

struct Passport {
    byr: String,
    iyr: String,
    eyr: String,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: Option<String>,
}

impl Passport {
    fn new (byr: String, iyr: String, eyr: String, hgt: String, hcl: String, ecl: String, pid: String, cid: Option<String>) -> Result<Passport, String> {
        Ok(Passport {byr, iyr, eyr, hgt, hcl, ecl, pid, cid})
    }
}

pub fn resolve (file: &str) -> Result<u32, String> {
    match file_to_vector(file) {
        Ok(p) => return Ok(p.iter().count() as u32),
        Err(_) => return Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Vec<Passport>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Passport> = Vec::new();

    let mut byr: Option<String>;
    let mut iyr: Option<String>;
    let mut eyr: Option<String>;
    let mut hgt: Option<String>;
    let mut hcl: Option<String>;
    let mut ecl: Option<String>;
    let mut pid: Option<String>;
    let mut cid: Option<String>;


    byr = None;
    iyr = None;
    eyr = None;
    hgt = None;
    hcl = None;
    ecl = None;
    pid = None;
    cid = None;

    for line in s.lines() {
        if line.chars().count() == 0 {
            if byr.is_some() && iyr.is_some() && eyr.is_some() && hgt.is_some() && hcl.is_some() && ecl.is_some() && pid.is_some() {
                vector.push(
                    Passport::new(
                        byr.unwrap(),
                        iyr.unwrap(),
                        eyr.unwrap(),
                        hgt.unwrap(),
                        hcl.unwrap(),
                        ecl.unwrap(),
                        pid.unwrap(),
                        cid
                    ).unwrap()
                );
            }

            byr = None;
            iyr = None;
            eyr = None;
            hgt = None;
            hcl = None;
            ecl = None;
            pid = None;
            cid = None;

            continue;
        }

        let data: Vec<&str> = line.split(' ').collect();
        for element in data {
            let split_element: Vec<&str> = element.split(':').collect();
            if split_element[1].chars().count() == 0 {
                continue;
            }

            match split_element[0] {
                "byr" => byr = Some(split_element[1].to_string()),
                "iyr" => iyr = Some(split_element[1].to_string()),
                "eyr" => eyr = Some(split_element[1].to_string()),
                "hgt" => hgt = Some(split_element[1].to_string()),
                "hcl" => hcl = Some(split_element[1].to_string()),
                "ecl" => ecl = Some(split_element[1].to_string()),
                "pid" => pid = Some(split_element[1].to_string()),
                "cid" => cid = Some(split_element[1].to_string()),
                _ => (),
            }
        }
    }

    if byr.is_some() && iyr.is_some() && eyr.is_some() && hgt.is_some() && ecl.is_some() && pid.is_some() {
        vector.push(
            Passport::new(byr.unwrap(), iyr.unwrap(), eyr.unwrap(), hgt.unwrap(), hcl.unwrap(), ecl.unwrap(), pid.unwrap(), cid).unwrap()
        );
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day04/example").is_ok());
    }

    #[test]
    fn resolution_is_2() {
        assert_eq!(2, resolve("./src/day04/example").unwrap());
    }
}
