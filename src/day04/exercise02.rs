use std::io;
use std::io::Read;
use std::fs::File;

use regex::Regex;

struct Passport {
    byr: String,
    iyr: String,
    eyr: String,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: Option<String>,
}

impl Passport {
    fn new (byr: String, iyr: String, eyr: String, hgt: String, hcl: String, ecl: String, pid: String, cid: Option<String>) -> Result<Passport, String> {
        match Passport::is_valid_or_error(byr.clone(), iyr.clone(), eyr.clone(), hgt.clone(), hcl.clone(), ecl.clone(), pid.clone()) {
            Ok(_) => return Ok(Passport {byr, iyr, eyr, hgt, hcl, ecl, pid, cid}),
            Err(e) => return Err(e),
        }
    }

    fn is_valid_or_error (byr: String, iyr: String, eyr: String, hgt: String, hcl: String, ecl: String, pid: String) -> Result<bool, String> {
        match byr.parse::<u32>() {
            Ok(b) => if b < 1920 || b > 2002 { return Err("Error".to_string()) },
            Err(_) => return Err("Error".to_string()),
        }

        match iyr.parse::<u32>() {
            Ok(b) => if b < 2010 || b > 2020 { return Err("Error".to_string()) },
            Err(_) => return Err("Error".to_string()),
        }

        match eyr.parse::<u32>() {
            Ok(b) => if b < 2020 || b > 2030 { return Err("Error".to_string()) },
            Err(_) => return Err("Error".to_string()),
        }

        let (number, unit) = hgt.split_at(hgt.chars().count() - 2);
        let hgt_number: u32 = number.parse::<u32>().unwrap();
        match unit {
            "cm" => if hgt_number < 150 || hgt_number > 193 { return Err("Error".to_string()) },
            "in" => if hgt_number < 59 || hgt_number > 76 { return Err("Error".to_string()) },
            _ => return Err("Error".to_string()),
        }

        let (number, unit) = hgt.split_at(hgt.chars().count() - 2);
        let hcl_re = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        if !hcl_re.is_match(hcl.as_str()) {
            return Err("Error".to_string());
        }

        let ecl_re = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
        if !ecl_re.is_match(ecl.as_str()) {
            return Err("Error".to_string());
        }

        let pid_re = Regex::new(r"^\d{9}$").unwrap();
        if !pid_re.is_match(pid.as_str()) {
            return Err("Error".to_string());
        }

        Ok(true)
    }
}

pub fn resolve (file: &str) -> Result<u32, String> {
    match file_to_vector(file) {
        Ok(p) => return Ok(p.iter().count() as u32),
        Err(_) => return Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Vec<Passport>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Passport> = Vec::new();

    let mut byr: Option<String>;
    let mut iyr: Option<String>;
    let mut eyr: Option<String>;
    let mut hgt: Option<String>;
    let mut hcl: Option<String>;
    let mut ecl: Option<String>;
    let mut pid: Option<String>;
    let mut cid: Option<String>;


    byr = None;
    iyr = None;
    eyr = None;
    hgt = None;
    hcl = None;
    ecl = None;
    pid = None;
    cid = None;

    for line in s.lines() {
        if line.chars().count() == 0 {
            if byr.is_some() && iyr.is_some() && eyr.is_some() && hgt.is_some() && hcl.is_some() && ecl.is_some() && pid.is_some() {
                match Passport::new(
                        byr.unwrap(),
                        iyr.unwrap(),
                        eyr.unwrap(),
                        hgt.unwrap(),
                        hcl.unwrap(),
                        ecl.unwrap(),
                        pid.unwrap(),
                        cid
                        ) {
                    Ok(p) => vector.push(p),
                    Err(_) => (),
                }
            }

            byr = None;
            iyr = None;
            eyr = None;
            hgt = None;
            hcl = None;
            ecl = None;
            pid = None;
            cid = None;

            continue;
        }

        let data: Vec<&str> = line.split(' ').collect();
        for element in data {
            let split_element: Vec<&str> = element.split(':').collect();
            if split_element[1].chars().count() == 0 {
                continue;
            }

            match split_element[0] {
                "byr" => byr = Some(split_element[1].to_string()),
                "iyr" => iyr = Some(split_element[1].to_string()),
                "eyr" => eyr = Some(split_element[1].to_string()),
                "hgt" => hgt = Some(split_element[1].to_string()),
                "hcl" => hcl = Some(split_element[1].to_string()),
                "ecl" => ecl = Some(split_element[1].to_string()),
                "pid" => pid = Some(split_element[1].to_string()),
                "cid" => cid = Some(split_element[1].to_string()),
                _ => (),
            }
        }
    }

    if byr.is_some() && iyr.is_some() && eyr.is_some() && hgt.is_some() && ecl.is_some() && pid.is_some() {
        match Passport::new(byr.unwrap(), iyr.unwrap(), eyr.unwrap(), hgt.unwrap(), hcl.unwrap(), ecl.unwrap(), pid.unwrap(), cid) {
            Ok(p) => vector.push(p),
            Err(_) => (),
        }
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day04/example").is_ok());
    }

    #[test]
    fn resolution_is_0() {
        assert_eq!(0, resolve("./src/day04/example2").unwrap());
    }

    #[test]
    fn resolution_is_4() {
        assert_eq!(4, resolve("./src/day04/example3").unwrap());
    }
}
