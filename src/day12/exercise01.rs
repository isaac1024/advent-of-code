use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

pub fn resolve (file: &str) -> Result<i32, String> {
    let map = file_to_vector(file).unwrap();

    let ns = map[&'N'] - map[&'S'];
    let we = map[&'W'] - map[&'E'];

    Ok(ns.abs() + we.abs())
}

fn file_to_vector(file: &str) -> Result<HashMap<char, i32>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut map: HashMap<char, i32> = [('N', 0), ('W', 0), ('S', 0), ('E', 0)].iter().cloned().collect();
    let mut orientation = 'E';

    for line in s.lines() {
        let (mut action, movement) = line.split_at(1);
        let mut n_movement = movement.parse::<i32>().unwrap();

        if action == "R" {
            action = "L";
            n_movement = 360 - n_movement;
        }

        match action {
            "F" => {
                let o = map.get_mut(&orientation).unwrap();
                *o += n_movement;
            },
            "L" => {
                while n_movement > 0 {
                    match orientation {
                        'N' => orientation = 'W',
                        'S' => orientation = 'E',
                        'E' => orientation = 'N',
                        'W' => orientation = 'S',
                        _ => (),
                    }

                    n_movement -= 90;
                }
            },
            _ => {
                let o = map.get_mut(&action.chars().next().unwrap()).unwrap();
                *o+= n_movement;
            },
        }
    }

    Ok(map)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day12/example").is_ok());
    }

    #[test]
    fn resolution_is_25() {
        assert_eq!(25, resolve("./src/day12/example").unwrap());
    }
}
