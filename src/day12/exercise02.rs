use std::io;
use std::io::Read;
use std::fs::File;

use std::collections::HashMap;

pub fn resolve (file: &str) -> Result<i32, String> {
    let map = file_to_vector(file).unwrap();

    let ns = map[&'N'] - map[&'S'];
    let we = map[&'W'] - map[&'E'];

    Ok(ns.abs() + we.abs())
}

fn file_to_vector(file: &str) -> Result<HashMap<char, i32>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut map: HashMap<char, i32> = [('N', 0), ('W', 0), ('S', 0), ('E', 0)].iter().cloned().collect();
    let mut orientation: HashMap<char, i32> = [('N', 1), ('W', 0), ('S', 0), ('E', 10)].iter().cloned().collect();

    for line in s.lines() {
        let (mut action, movement) = line.split_at(1);
        let mut n_movement = movement.parse::<i32>().unwrap();

        if action == "L" {
            action = "R";
            n_movement = 360 - n_movement;
        }

        match action {
            "F" => {
                for (or, pos) in orientation.iter() {
                    let o = map.get_mut(or).unwrap();
                    *o += pos*n_movement;
                }
            },
            "R" => {
                while n_movement > 0 {
                    let n = orientation.get(&'N').unwrap().clone();
                    let w = orientation.get(&'W').unwrap().clone();
                    let s = orientation.get(&'S').unwrap().clone();
                    let e = orientation.get(&'E').unwrap().clone();

                    for key in map.keys() {
                        let pos: i32 = match key {
                            'E' => n,
                            'N' => w,
                            'W' => s,
                            'S' => e,
                            _ => 0,
                        };

                        let o = orientation.get_mut(key).unwrap();
                        *o = pos;
                    }

                    n_movement -= 90;
                }
            },
            _ => {
                let o = orientation.get_mut(&action.chars().next().unwrap()).unwrap();
                *o+= n_movement;
            },
        }
    }

    Ok(map)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day12/example").is_ok());
    }

    #[test]
    fn resolution_is_286() {
        assert_eq!(286, resolve("./src/day12/example").unwrap());
    }
}
