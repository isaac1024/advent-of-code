use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u32, String> {
    match file_to_vector(file) {
        Ok(v) => return Ok(v.iter().map(|x: &Vec<char>| x.iter().count() as u32).sum()),
        Err(_) => Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Vec<Vec<char>>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Vec<char>> = Vec::new();
    let mut letters: Vec<char> = Vec::new();
    for line in s.lines() {
        if line.is_empty() {
            vector.push(letters);
            letters = Vec::new();

            continue;
        }

        for character in line.chars() {
            if !letters.contains(&character) {
                letters.push(character);
            }
        }
    }

    vector.push(letters);

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day06/example").is_ok());
    }

    #[test]
    fn resolution_is_11() {
        assert_eq!(11, resolve("./src/day06/example").unwrap());
    }
}
