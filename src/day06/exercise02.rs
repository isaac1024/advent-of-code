use std::io;
use std::io::Read;
use std::fs::File;

pub fn resolve (file: &str) -> Result<u32, String> {
    match file_to_vector(file) {
        Ok(v) => return Ok(v.iter().map(|x: &String| x.chars().count() as u32).sum()),
        Err(_) => Err("Error".to_string()),
    }
}

fn file_to_vector(file: &str) -> Result<Vec<String>, io::Error> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<String> = Vec::new();
    let mut letters: String = String::new();
    let mut empty: bool = true;
    for line in s.lines() {
        if line.is_empty() {
            vector.push(letters);
            letters = String::new();
            empty = true;

            continue;
        }

        if empty {
            letters.push_str(line);
            empty = false;
        } else {
            let mut aux: String = letters.clone();
            for character in letters.chars() {
                if !line.contains(character) {
                    aux = aux.replace(character, "");
                }
            }
            letters = aux;
        }
    }

    vector.push(letters);

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day06/example").is_ok());
    }

    #[test]
    fn resolution_is_6() {
        assert_eq!(6, resolve("./src/day06/example").unwrap());
    }
}
