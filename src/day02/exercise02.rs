use std::error::Error;
use std::io::Read;
use std::fs::File;

struct Password {
    value: String,
}

impl<'a> Password {
    fn new(password: &'a str, control: char, min: u32, max: u32) -> Result<Password, &'a str> {
        match Password::is_valid_or_faild(password, control, min, max) {
            Ok(_) => return Ok(Password {value: password.to_string()}),
            Err(e) => return Err(e),
        }
    }

    fn is_valid_or_faild(password: &'a str, control: char, min: u32, max: u32) -> Result<&'a str, &'a str> {
        let first_char: bool = password.chars().nth(min as usize - 1) == Some(control);
        let second_char: bool = password.chars().nth(max as usize - 1) == Some(control);
        if first_char == second_char {
            return Err("Invalid password");
        }

        Ok(password)
    }
}

pub fn resolve (file: &str) -> Result<u32, Box<dyn Error>> {
    Ok(file_to_vector(file)?.iter().count() as u32)
}

fn file_to_vector(file: &str) -> Result<Vec<Password>, Box<dyn Error>> {
    let mut s = String::new();

    File::open(file)?.read_to_string(&mut s)?;

    let mut vector: Vec<Password> = Vec::new();
    for line in s.lines() {
        let password_vector: Vec<&str> = line.split(|c| c == '-' || c == ' ').collect();

        let min = password_vector[0].parse::<u32>()?;
        let max = password_vector[1].parse::<u32>()?;
        let control = match password_vector[2].chars().nth(0) {
            Some(c) => c,
            None => return Err("Error".into()),
        };
        let password = password_vector[3];
        match Password::new(password, control, min, max) {
            Ok(p) => vector.push(p),
            Err(_) => continue,
        }
    }

    Ok(vector)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn if_file_not_exist_return_error() {
        assert!(file_to_vector("file_not_exist").is_err());
    }

    #[test]
    fn if_file_exist_return_ok() {
        assert!(file_to_vector("./src/day02/example").is_ok());
    }

    #[test]
    fn resolution_is_2() -> Result<(), Box<dyn Error>> {
        assert_eq!(1, resolve("./src/day02/example")?);
        Ok(())
    }
}
